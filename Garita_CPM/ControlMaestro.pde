/*
Es necesario que exista la siguiente variable global
 
 int valDir=0;//recibe el valor de Arduino
 
 
 Esta clase consta de 
 
 > constructor(es)
 ControlMaestro()//parametros y carga de imagenes de clase CargaImagenes
 
 > metodos publicos:
 void dibuja()//metodo principal de acceso a la clase
 void keyPressed()// eventos de teclado que emulan los sensores y de acceso al debug 
 
 > metodos privados:
 void rutinas()// evalua y setea la rutina actual
 void reproduce()// metodo de control sobre la animación de las imagenes y toma de decisión
 void debug()//
 void lecturasPIR()// lectura de los eventos de los sensores.
 
 _Krista Yorbyck 25/03/2020
 */

class ControlMaestro
{

  CargaImagenes [] imaZero= new CargaImagenes [3];
  CargaImagenes [] imaPresencia= new CargaImagenes [1];

  int num, cualZero, cualRutina;
  boolean correRutina, rutinaZero=true, 
    rutinaIzq, rutinaDer, evalua, debuggeo=true, ocultaImg=false;
  int evalTime, evalLim=60;

  ControlMaestro()
  {
    //  CargaImagenes(String ruta_, String extencion_, int #inicio_, int cantIma_, int cantCeros_ )

    imaZero [0]= new CargaImagenes("data/zero/zeroUno/zeroUno_", ".png", 0, 90, 4);
    imaZero [1]= new CargaImagenes("data/zero/zeroDos/zeroDos_", ".png", 0, 90, 4);
    imaZero [2]= new CargaImagenes("data/zero/zeroTres/zeroTres_", ".png", 0, 90, 4);
    imaPresencia [0]= new CargaImagenes("data/presencia/presenciaUno/presenciaUno_", ".png", 0, 90, 4);
    //  imaIzq= new CargaImagenes("data/izq/izq_", ".png", 0, 90, 4);
  }


  public void dibuja()
  {
    lecturaPIR();
    reproduce();
    switch (cualRutina) {
    case 0:    
      image(imaZero[cualZero].getImagen(num), 0, 0, width, height);
      break;
    case 1:    
      image(imaPresencia[0].getImagen(num), 0, 0, width, height);
      break;
    case 2: 
      image(imaPresencia[0].getImagen(num), 0, 0, width, height);
      //   ellipse(width/2, height/2, 400, 400);
      break;
    }

    if (ocultaImg)
    {
      fill(0);
      rect(0, 0, width, height);
    }
    debug();
  }

  private void rutinas()
  {

    if (rutinaZero)
    {
      cualZero= int(random(2));
    }
  }


  private void reproduce()
  {
    //if (num==89)num=0; 
    //else num++;

    if (num==89)
    {
      num=0; 
      rutinaZero=false;
      rutinaDer=false;
      rutinaIzq= false;
      cualRutina=0;
      evalua=true;
    }

    //  if (num==0)evalua=true;

    if (evalua==true && evalTime<evalLim)
    {

      if (rutinaDer==true)
      {
        cualRutina=1;
      }

      if (rutinaIzq==true)
      {
        cualRutina=2;
      }
      evalTime++;
    }

    if (evalTime==evalLim)
    {
      evalua=false;
      evalTime=0;
    }

    if (evalua==false && num==0)
    {
      cualZero=int (random(3));
      rutinaZero=true;
    }

    if (rutinaZero || rutinaIzq || rutinaDer)
    {
      num++;
    }
  }


  //-------------------> Debug

  private void debug() {
    if (debuggeo)
    {
      cursor();
      pushStyle();
      if (evalua)
      { 
        rectMode(CORNER);
        fill(150, 0, 30, 100);
        rect(0, 50, width, height/4);

        if (rutinaIzq)
        {

          square(50, height/2, width/4);
        }

        if (rutinaDer)
        {
          square(width-150, height/2, width/4);
        }

        textSize(30);
        fill(220);
        text("Izquierda: a", 25, 140);
        text("Derecha: d", 25, 190);
        text("toma de Decision", 25, 90);
      }
      fill(220);
      textSize(30);
      text("sensorNUM \n "+valDir, width/2, height/2);

      textSize(20);
      fill(255);
      text("rutinaZero: " + cualZero, 20, height-(height/6));
      fill(150, 150, 30);
      text("fotograma num: "+num, 20, height-(height/6)+30);
      text("fps: "+frameRate, 20, height-(height/6)+60);

      textSize(15);
      fill(255);
      text("Exit DEBUG \n ' ENTER ' ", (width/2)+25, height-(height/6));
      text("Oculta animación\n ' ESPACIO ' ", (width/2)+25, height-(height/6)+70);

      popStyle();
    }
    else
    {
      noCursor();
    }
  }

  private void lecturaPIR()
  {
    switch(valDir) {
    case 1: 
      rutinaIzq=true;
      break;
    case 2: 
      rutinaDer=true;
      break;
    }
  }

  public void keyPressed()
  {
    if (key=='d')
    {
      rutinaDer=true;
    }
    if (key=='a')
    {
      rutinaIzq= true;
    }

    if (key=='p')
    {
      if (cualZero==imaZero.length-1)cualZero=0; 
      else cualZero++;
    }

    if (keyCode== 10)// Código ASCII 32==ENTER
    {
      debuggeo= !debuggeo;
    }

    if (keyCode== 32)// Código ASCII 32==SPACE
    {
      ocultaImg= !ocultaImg;
    }
  }
}//cierra ControlMaestro
