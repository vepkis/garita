class CargaImagenes
{
 private  ArrayList <PImage> ima= new ArrayList<PImage>();
  
  CargaImagenes(String ruta_, String extencion_, int inicio_,int cantIma_, int cantCeros_ )
  {
    println("\n");
    println("___ Iniciando Carga de imagenes ___");
    println("\n");
    for (int i=inicio_; i<inicio_+cantIma_; i++)
    {

      ima.add(parametros2(ruta_, extencion_, i, cantCeros_));


      println("- ! "+i +" !  -");
    }
    println("\n");
    println("imagenes cargadas____"+ima.size());
    println("___ Finalizo Carga de imagenes ___");
    println("\n");
  }

  PImage parametros(String ruta_, String extencion_, int num_, int cantCeros_)
  {
    PImage neu= loadImage(ruta_+trim(nfs(num_, cantCeros_))+extencion_);
    return neu;
  }

  PImage parametros2(String ruta_, String extencion_, int num_, int cantCeros_)
  {
    PImage neu= requestImage(ruta_+trim(nfs(num_, cantCeros_))+extencion_);
    return neu;
  }

//  void dibuja(int posA, int posB, int posC, int posD)
//  {
//    image(imaGeneral.getImagen(5), 0, 0, 100, 100);
//  }

  //-----------------------v G E T T E R S

  PImage getImagen(int index_)
  {
    return ima.get(index_);
  }

  int getCant()
  {
    return ima.size();
  }
}// cierra CargaImagenes 
