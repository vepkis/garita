/* Programa realizado por el medialab "LAIT" 
 para la comisión provincial por la memoria "CPM"
 
 _Programadora
 _Krista Yorbyck
 Año 2020
 */


import processing.serial.*;

ControlMaestro cMaestro;

int valDir=0;//recibe el valor de Arduino

PImage fondo;

void settings() {

  //fullScreen(P2D);
  //size(450, 800);
  size (1080, 1920);
}

void setup ()
{
  cursor(WAIT);
  setupSerial();
  println("--- Iniciando Calibración de Sensores ---");
  delay(20000);
  println("--- Iniciando carga de Imagenes ---");
  delay(1000);
  fondo= loadImage("fondo.jpg");
  cMaestro= new ControlMaestro();
  frameRate(30);
}


void draw ()

{
  image(fondo, 0, 0, width, height);
  //  background(0);
  cMaestro.dibuja();
  filtro();
}

void filtro()
{
  pushStyle();
  fill(200, 150, 10, 20);
  rect(0, 0, width, height);
  // tint(200, 150, 10, 200);
  popStyle();
}

void keyPressed()
{
  cMaestro.keyPressed();
}
