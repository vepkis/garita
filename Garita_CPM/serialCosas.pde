/*
funciones Serial que recogen los valores de los sensores PIR
desde el arduino.
*/

void serialEvent(Serial myPort) {
  // read the serial buffer:
  String myString = myPort.readStringUntil('\n');
  // if you got any bytes other than the linefeed:
  println(myString);
  int val22 []= int(split(myString, ';'));
  valDir= val22[0];

  println("valor--->"+valDir);
}

void setupSerial()
{
  // Lista de puertos disponibles
  printArray(Serial.list());
  Serial myPort;

  for (int i = 0; i < Serial.list().length; i++) {  
    String portName = Serial.list()[i];
    String selectPort;
    if (portName.contains("USB") || portName.contains("ACM") || (portName.contains("tty") && portName.contains("usb"))) {
      myPort = new Serial(this, portName, 9600);
      myPort.clear();
      myPort.bufferUntil('\n');
      println("cual===> "+portName);
      println("---Puerto cargado---");
      break;
    }
    else
    {
     println("Conect-á el Arduino "); 
    }
  }

  //return myPort;
}


//Serial setupSerial(int puerto_)
//{
//  // Lista de puertos disponibles
//  printArray(Serial.list());
//  Serial myPort;
//  //coloar el index del puerto serial correspondiente
//  String portName = Serial.list()[puerto_];
//  myPort = new Serial(this, portName, 9600);
//  //limpiar el puerto serial, para evitar lecturas cargadas.
//  myPort.clear();
//  myPort.bufferUntil('\n');

//  println("---Puerto cargado---");

//  return myPort;
//}
