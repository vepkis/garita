const int LEDPin = 13;        // pin para el LED
const int PIRPin [2] = {8, 10};         // pin de entrada (for PIR sensor)
int val [2] = {0, 0};
int pirState [2] = {LOW, LOW};          // de inicio no hay movimiento
int dir = 0;

//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 10;  

void setup()
{
  pinMode(LEDPin, OUTPUT);
  pinMode(PIRPin, INPUT);
  Serial.begin(9600);
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
 //give the sensor some time to calibrate
  Serial.print("calibrating sensor ");
    for(int i = 0; i < calibrationTime; i++){
      Serial.print(".");
      delay(1000);
      }
    Serial.println(" jetzt ");
    Serial.println("Lets go");
    delay(50);
 
}

void loop()
{
  val[0] = digitalRead(PIRPin[0]);
  val[1] = digitalRead(PIRPin[1]);
  if (val[0] == HIGH)   //si está activado
  {
    digitalWrite(LEDPin, HIGH);  //LED ON
    if (pirState[0] == LOW)  //si previamente estaba apagado
    {
      pirState[0] = HIGH;
      dir = 2;
      Serial.print(dir, DEC);
      Serial.println(";");

    }
  }
  else   //si esta desactivado
  {
    if (pirState[0] == HIGH)  //si previamente estaba encendido
    {
    digitalWrite(LEDPin, LOW); // LED OFF

      pirState[0] = LOW;
      dir = 0;
      Serial.print(dir, DEC);
      Serial.println(";");

    }
  }

  if (val[1] == HIGH)   //si está activado
  {
    digitalWrite(LEDPin, HIGH);  //LED ON
    if (pirState[1] == LOW)  //si previamente estaba apagado
    {
      pirState[1] = HIGH;
      dir = 1;
      Serial.print(dir, DEC);
      Serial.println(";");
    }
  }
  else   //si esta desactivado
  {
    if (pirState[1] == HIGH)  //si previamente estaba encendido
    {
    digitalWrite(LEDPin, LOW); // LED OFF

      pirState[1] = LOW;
      dir = 0;
      Serial.print(dir, DEC);
      Serial.println(";");
    }
  }

 // delay(10);


}
